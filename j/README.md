# JAPL

![J Looks Like APL](./japl.png)


## Not your daddy's J
Well, technically it is. The actual text in  [the source](./15.ijs) appears as normal:
```ijs
NB. https://adventofcode.com/2019/day/1#part2
inp1 =.  0 ". >cutopen read <'../2019/1.txt'
ans1 =. 3317668 4973628

Day1 =. dyad define
    fuel =. 2-~<.@%&3
    S1   =. +/@ fuel 
    S2   =. +/@,@}.@((0 >. fuel)^:a:)
    (y { S1`S2)  run  x
)
```

By taking inspiration from [the original](https://wjmn.github.io/posts/j-can-look-like-apl/), and through not so clever use of Visual Studio code extensions, you too can have nice looking APL glyphs to "conceal" or "mask" your J code, without altering the source itself. This is merely a cosmetic effect that works with Visual Studio Code, that can be [achieved using other editors as well](#original-author) but is not the scope of this project. 

## Installation
Installation instructions are very simple, simply follow the `"instructions:[]` found [here](./japl.json). That link contains all the instructions, and configurations you'll need to get started Prettifying your J code.


## Original Author
This isn't my original idea. The original idea came from a [video demonstration](https://www.youtube.com/watch?v=jTeENcpUZdM) of this same idea executed using emacs. I reached out to the original author asking for his emacs configurations, but his response only linked back to the original article, so I decided to try myself. 