(in-package #:aoc)

(defvar *year* nil)

(defun abs-path (path)
  (asdf:system-relative-pathname
   (intern (package-name *package*) "KEYWORD")
   path))

(defun load-apl (file)
  (eval `(april-load ,(pathname (abs-path file)))))

(defun set-year (year)
  (let ((year-string (write-to-string year)))
    (setf *year* (parse-integer (subseq year-string
                           (- (length year-string) 2)
                           (length year-string))))
    (let ((symbol (intern (format nil "aoc~a" *year*))))
      (format t "~%~%(⊢∘○∘)⊢ Loading solutions from year 20~a... t(∘○∘t)~%~%" *year*)
      (eval `(do-set-year ,symbol)))))

(defmacro do-set-year (symbol)
  `(april-load
    (with (:space ,symbol))
    ,(abs-path (format nil "./apl/~a.apl" *year*))))


(defun prompt (day)
  (prompt-for-day (format nil "20~d" *year*) day))

(defmacro day (day &rest parser)
  (set-year *year*)
  (let* ((sym 'input)
         (value (if (null parser)
                    sym
                    `(progn ,@parser))))
    `(let* ((,sym (input-for-day ,(format nil "20~d" *year*) ,day))
            (inp ,value))
       (april-c
        (with (:space ,(format nil "aoc~a" *year*)))
        ,(format nil "day~d" day)
                (to-array inp)))))
