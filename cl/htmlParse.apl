 parseHtml←{

     ⍝ remove comment tags
     nb ← {1+⍵+'>'⍳⍨⍵⌽⍺} ⍝ [n]ext [b]racket
     rc ← {              ⍝ [r]emove [c]omments
         o ← ⍸(⊂'<!')≡¨⍵
         c ← ⍺∘nb¨o
         r ← ⍳≢⍺
         (~+\(¯1×r∊c)+r∊o)/⍺
     }
     x←⊃,/1↓⍵

     ⍝ nest
     y←x rc 2,/x

     s←(⊂'/>')≡(⊂¯2∘↑) ⍝ [s]elf closing tag
     c←(⊂'</')≡(⊂2∘↑)  ⍝ [c]losing tag
     o←(~c)∧('<'=⊃)    ⍝ [o]pening tag
     split←' '∘(≠⊆⊢)   ⍝ string [split] on space

     ⍝ recursively earch for open and closing tags
     ⍝ keeping track ot the body separately
     type ← 'em'    'strong' 'pre'    'code'  'li'     'h'     'a'
     frmt ← '*'     '**'     '```'    '`'     '-'      '#'     ''      ''
     rule ← 'quote' 'quote'  'quote'  'quote' 'prefix' 'prefix' 'link'  'default'
     rec  ← {
         open←{tag←⍵[⍳next←⍵⍳'>']
               parts←split tag
               name←(⊃parts)~'<>'
             $[name≡'script';"",⍺ skip next↓⍵; ⍺{
                 rest←1↓parts
                 newDepth←⍺+~s tag
                 i←type⍳name
                 f←,frmt[i]
                 r←,rule[i]
                ⎕←name
                ⎕←f
                 result←newDepth rec next↓⍵
                 ⎕←result
                 $[r≡'quote';f,f,⍨result; f,' ',result]

             }⍵]
         }

         skip←{"", ⍺ rec ⍵↓⍨¯1+⍵⍳'<'}

         close←{tag←⍵[⍳next←⍵⍳'>']
                "", ⍺ rec next↓⍵}


         body←{content←⍵[⍳next←¯1+⍵⍳'<']
             ⎕←content
               content, ⍺ rec next↓⍵}

         $[0=≢⍵;"";
           o ⍵ ;  ⍺    open  ⍵;
           c ⍵ ; (⍺+1) close ⍵;
                 (⍺+1) body  ⍵]
     }

     (⍳10)[1+⍳5]
     r←0 rec y
     ⎕←r

     r⊆y
     ⍝ c←{(⍳≢y)∊y∘nb¨⍵}⍸(⊂'</')∊⍨2,/y ⍝ [c]lose
     ⍝ s←(⊂'/>')≡¨2,/y                ⍝ [s]elf-closing
     ⍝ o←{y∘nb¨⍵}⍸{('<'≡⊃⍵)∧'/'≢⊃⌽⍵}¨2,/y
     ⍝ ⍴¨c s o
     ⍝ ⎕←y,⍪z←+\(¯1×c∨s,0)+o,0

     ⍝t←'article'
     ⍝p←(2⌽1=+\t⍷x)/x
     ⍝p,'</',t,'>'
 }